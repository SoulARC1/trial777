package org.example.database;

import org.example.model.Calendar;
import org.example.model.Division;
import org.example.model.Event;
import org.example.model.User;

import java.util.HashMap;

public class CalendarDatabase {
    private HashMap<Long, User> userDB;
    private HashMap<Long, Division> divisionDB;
    private HashMap<Long, Event> eventDB;
    private HashMap<Long, Calendar> calendarDB;
}
