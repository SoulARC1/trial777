package org.example.service.impl;

import org.example.model.Division;
import org.example.model.Event;
import org.example.model.User;
import org.example.service.EventService;

import java.util.List;

public class EventServiceImpl implements EventService {
    @Override
    public Event create(Event element) {
        return null;
    }

    @Override
    public Event get(Long id) {
        return null;
    }

    @Override
    public List<Event> getAll() {
        return null;
    }

    @Override
    public Event update(Event element) {
        return null;
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }

    @Override
    public boolean addUserToEvent(User user, Event event) {
        return false;
    }

    @Override
    public boolean addToAllByDivision(Division division, Event event) {
        return false;
    }

    @Override
    public boolean removeUserFromEvent(User user, Event event) {
        return false;
    }

    @Override
    public List<Event> getAllEventByUser(User user) {
        return null;
    }

    @Override
    public List<Event> getAllEventByDivision(Division division) {
        return null;
    }
}
