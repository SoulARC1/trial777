package org.example.service.impl;

import org.example.dao.DivisionDao;
import org.example.dao.UserDao;
import org.example.model.Division;
import org.example.model.User;
import org.example.service.DivisionService;

import java.util.List;

public class DivisionServiceImpl implements DivisionService {
    private final DivisionDao divisionDao;
    private final UserDao userDao;

    public DivisionServiceImpl(DivisionDao divisionDao, UserDao userDao) {
        this.divisionDao = divisionDao;
        this.userDao = userDao;
    }

    @Override
    public Division create(Division element) {
        return null;
    }

    @Override
    public Division get(Long id) {
        return null;
    }

    @Override
    public List<Division> getAll() {
        return null;
    }

    @Override
    public Division update(Division element) {
        return null;
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }

    @Override
    public List<Division> getAllDivisionByUser(User user) {
        return null;
    }
}
