package org.example.service;

import org.example.model.Division;
import org.example.model.User;

import java.util.List;

public interface DivisionService extends GeneralCrudService<Division> {
    List<Division> getAllDivisionByUser (User user);
}
