package org.example.service;

import org.example.model.Event;

public interface CalendarService {
    void addEvent(Event event);
    void removeEvent(Long id);
}
