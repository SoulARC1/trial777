package org.example.dao;

import org.example.model.Event;

public interface CalendarDao {
    void addEvent(Event event);
    void removeEvent(Long id);}
