package org.example.dao;

import org.example.model.Division;
import org.example.model.Event;
import org.example.model.User;

import java.util.List;

public interface UserDao extends GeneralCrudDao<User>{
    boolean addEvenToUser(Event event, User user);

    boolean removeEventFromUser(Event event, User user);

    boolean addDivisionToUser(Division division, User user);

    boolean removeDivisionFromUser(Division division, User user);

    List<User> getAllUserByDivision(Division division);

    List<User> getAllUserBuEvent(Event event);
}
