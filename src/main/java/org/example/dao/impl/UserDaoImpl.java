package org.example.dao.impl;

import org.example.dao.UserDao;
import org.example.model.Division;
import org.example.model.Event;
import org.example.model.User;

import java.util.List;
import java.util.Optional;

public class UserDaoImpl implements UserDao {
    @Override
    public User create(User element) {
        return null;
    }

    @Override
    public Optional<User> get(Long id) {
        return Optional.empty();
    }

    @Override
    public List<User> getAll() {
        return null;
    }

    @Override
    public User update(User element) {
        return null;
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }

    @Override
    public boolean addEvenToUser(Event event, User user) {
        return false;
    }

    @Override
    public boolean removeEventFromUser(Event event, User user) {
        return false;
    }

    @Override
    public boolean addDivisionToUser(Division division, User user) {
        return false;
    }

    @Override
    public boolean removeDivisionFromUser(Division division, User user) {
        return false;
    }

    @Override
    public List<User> getAllUserByDivision(Division division) {
        return null;
    }

    @Override
    public List<User> getAllUserBuEvent(Event event) {
        return null;
    }
}
