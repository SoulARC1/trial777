package org.example.dao.impl;

import org.example.dao.DivisionDao;
import org.example.model.Division;
import org.example.model.User;

import java.util.List;
import java.util.Optional;

public class DivisionDaoImpl implements DivisionDao {
    @Override
    public Division create(Division element) {
        return null;
    }

    @Override
    public Optional<Division> get(Long id) {
        return Optional.empty();
    }

    @Override
    public List<Division> getAll() {
        return null;
    }

    @Override
    public Division update(Division element) {
        return null;
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }

    @Override
    public List<Division> getAllDivisionByUser(User user) {
        return null;
    }
}
