package org.example.dao.impl;

import org.example.dao.EventDao;
import org.example.model.Division;
import org.example.model.Event;
import org.example.model.User;

import java.util.List;
import java.util.Optional;

public class EventDaoImpl implements EventDao {
    @Override
    public Event create(Event element) {
        return null;
    }

    @Override
    public Optional<Event> get(Long id) {
        return Optional.empty();
    }

    @Override
    public List<Event> getAll() {
        return null;
    }

    @Override
    public Event update(Event element) {
        return null;
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }

    @Override
    public boolean addUserToEvent(User user, Event event) {
        return false;
    }

    @Override
    public boolean addToAllByDivision(Division division, Event event) {
        return false;
    }

    @Override
    public boolean removeUserFromEvent(User user, Event event) {
        return false;
    }

    @Override
    public List<Event> getAllEventByUser(User user) {
        return null;
    }

    @Override
    public List<Event> getAllEventByDivision(Division division) {
        return null;
    }
}
