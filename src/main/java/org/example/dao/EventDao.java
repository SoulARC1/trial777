package org.example.dao;

import org.example.model.Division;
import org.example.model.Event;
import org.example.model.User;

import java.util.List;

public interface EventDao extends GeneralCrudDao<Event>{
    boolean addUserToEvent(User user, Event event);

    boolean addToAllByDivision(Division division, Event event);

    boolean removeUserFromEvent(User user, Event event);

    List<Event> getAllEventByUser(User user);

    List<Event> getAllEventByDivision(Division division);
}
