package org.example.dao;

import org.example.model.Division;
import org.example.model.User;

import java.util.List;

public interface DivisionDao extends GeneralCrudDao<Division>{
    List<Division> getAllDivisionByUser (User user);

}
