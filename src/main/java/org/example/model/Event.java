package org.example.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Event {
    private long id;
    private String name;
    private String description;
    private List<User> users;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
}
